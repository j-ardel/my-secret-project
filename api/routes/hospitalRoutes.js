'use strict';
module.exports = function (app) {
  const hospitalController = require('../controllers/hospitalController');

  app.route('/hospital/closest').get(hospitalController.calculateTime);
};
